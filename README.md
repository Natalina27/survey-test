# DUOS Survey Exercise

Using the provided UI mocks, and the mock apis below,
implement an API-driven survey UI using React (or your preferred Javascript framework).

Survey JSON format:
```json
{
 "title": "string",
 "description": "string",
 "sections": [
                {
                  "title": "string",
                 "questionIds": [ "uuid" ]
                }
  
              ]
}

```

Question JSON format:
```json
{
  "questionText": "string",
  "isRequired": "boolean"
}
```
At the end of the survey,
submit the responses to the POST endpoint below in the following format:
```json
{
  "surveyId": "889278b3-e56f-4a20-adef-9f601a363524",
  "responses": [
                      {
                        "questionId": "uuid",
                        "responseText": "string"
                        }
                ]
}
```


Mock API endpoints to use for implementation:

- GET survey json:
> https://run.mocky.io/v3/e56a3256-8703-4705-a182-77cad0b702d1
- GET each question json:
> https://run.mocky.io/v3/question uuid
- POST survey responses json:
> https://run.mocky.io/v3/b081763d-eef1-44ab-8bab-3ce008c49bdf

### UI mock:
![Survey Start Page](src/assets/survey-start.png)
![Survey Start Page](src/assets/survey-qa.png)
![Survey Start Page](src/assets/survey-finish.png)


### Checkmark icon:
<svg xmlns="http://www.w3.org/2000/svg" width="200" height="200" viewBox="0 0 160 160" fill="none">
    <path
            d="M53.334 83.3335L73.334 103.333L106.667 63.3335"
            stroke="#AC3EA4"
            stroke-width="10"
            stroke-linecap="round"
            stroke-linejoin="round"
    />
    <path
            d="M80.0007 146.667C116.82 146.667 146.667 116.819 146.667 80.0002C146.667 43.1812 116.82 13.3335 80.0007 13.3335C43.1817 13.3335 13.334 43.1812 13.334 80.0002C13.334 116.819 43.1817 146.667 80.0007 146.667Z"
            stroke="#AC3EA4"
            stroke-width="10"
    />
</svg>


Bonus points: implement a progress bar to indicate survey progress (not required).
