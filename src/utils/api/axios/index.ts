import axios from 'axios';
import { root } from '../config';
import {IData, ISurveyData} from '../../../types';

const axiosInstance: any = axios.create({
    baseURL: root,
    headers: {
        'Content-Type': 'application/JSON',
    }

});

export const api = Object.freeze({
    getSurvey: async (): Promise<ISurveyData> => {
        const { data } = await axiosInstance.get(`/e56a3256-8703-4705-a182-77cad0b702d1`);
        return data;
    },

    getQuestionById: async(id: string) => {
        const { data } = await axiosInstance.get(`/${id}`);
        return data;
    },

    postSurvey: async (postData: IData) => {
        const response  = await axiosInstance.post(`/b081763d-eef1-44ab-8bab-3ce008c49bdf`, JSON.stringify({postData}));
        return await response.data;
    }
});
