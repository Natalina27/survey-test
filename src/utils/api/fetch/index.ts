import { root } from '../config';
import {IData, ISurveyData} from '../../../types';

export const api = Object.freeze({
    getSurvey: async (): Promise<ISurveyData> => {
        const response =  await fetch(`${root}/e56a3256-8703-4705-a182-77cad0b702d1`);
        return await response.json();
        },

    getQuestionById: async(id: string) => {
        const response = await fetch(`${root}/${id}`);
        return await response.json();
    },

    postSurvey: async (data: IData) => {
        const response = await fetch(`${root}/b081763d-eef1-44ab-8bab-3ce008c49bdf`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({data})
        });
        return await response.json();
    }
})
