export interface IResult {
    question: string
    answer: string
}

export interface IData {
    surveyId: string
    responses: IResult[]
}

export interface ISurveyData {
    surveyName?: string
    description?: string
    sections: {
        title: string
        questionIds: string[]
    }[]
}

export interface IQuestion {
    section: string
    questionId: string
}

export type IQuestions = IQuestion[]



