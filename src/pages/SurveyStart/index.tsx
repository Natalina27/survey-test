import React, { FC } from 'react';
import {MainButton} from '../../components';
import styles from './style.module.scss';

interface ISurveyStart {
    surveyName?: string,
    description?: string,
    onStart: () => void,
}

export const SurveyStart: FC <ISurveyStart> = (
    {surveyName = 'Survey Name', description = 'Description', onStart}) => {
    return (
        <>
            <p className={styles.title}> {surveyName}</p>
            <p>{description}</p>
           <MainButton children='START SURVEY' onClick={onStart}/>
        </>
    );
};

