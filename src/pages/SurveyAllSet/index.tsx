import React from 'react';
import {ReactComponent as IconSVG} from '../../assets/checkmark-icon.svg';

export const SurveyAllSet = () => {
    return (
        <>
            <h1>All set!</h1>
            <IconSVG/>
            <p>Thank you for taking out survey!</p>
        </>
    );
};
