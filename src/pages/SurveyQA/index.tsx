import React, {FC, useEffect, useState} from 'react';
import {api} from '../../utils/api';
import {v4 as uuidv4} from 'uuid';
import {BackButton, Input, MainButton, ProgressBar} from '../../components';
import {IQuestions, IResult} from '../../types';

interface ISurveyQA {
    questions: IQuestions,
    setDone: (done: boolean) => void,
}

export const SurveyQA: FC<ISurveyQA> = ({questions, setDone}) => {
    const [questionIdx, setQuestionIdx] = useState(0);
    const [question, setQuestion] = useState('');
    const [isRequired, setIsRequired] = useState(false);
    const [value, setValue] = useState('');
    const [results, setResults] = useState<IResult[]>([]);
    const [surveyId] = useState(uuidv4());
    const [error, setError] = useState(false);
    const [placeholder, setPlaceholder] = useState('');

    useEffect(() => {
        (async () => {
            const data = await api.getQuestionById(questions[questionIdx].questionId);
            setQuestion(data.questionText);
            setIsRequired(data.isRequired);
            setPlaceholder(`Answer ${data.isRequired ? 'is' : 'isn\'t '} required`);

            await api.postSurvey({
                surveyId,
                responses: results
            });
        })();
        setPlaceholder(`Answer ${isRequired ? 'is' : 'isn\'t '} required`);
    }, [questionIdx]);

    const onMainButtonClick = () => {

        if (isRequired && !value) {
            setError(true);
            setPlaceholder('Answer is required');
            return;
        }

        setResults([...results, {question: questions[questionIdx].questionId, answer: value}]);
        if (questionIdx < questions.length - 1) {
            setQuestionIdx(questionIdx + 1);
        } else {
            setDone(true);
        }
        setValue('');
        setError(false);
    }

    return (
        <>
            <BackButton/>
            <h2>{questions[questionIdx].section}</h2>
            <h3>{question}</h3>
            <Input value={value}
                   handleChange={(e) => setValue(e.target.value)}
                   error={error}
                   placeholder={placeholder}/>
            <MainButton onClick={onMainButtonClick} children='NEXT'/>
            <ProgressBar completed={Math.round(questionIdx / questions.length * 100)}/>
        </>
    );
}
