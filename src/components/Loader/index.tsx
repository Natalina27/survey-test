import React from 'react';
import styles from './style.module.scss'

export const Loader = () => {
    return (
        <div className={styles.loader}>
            Loading......
        </div>
    );
};
