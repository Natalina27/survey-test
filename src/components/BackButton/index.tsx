import React from 'react';
import styles from './style.module.scss'

export const BackButton = () => {
    const goBack = () => window.location.href = '/';

    return (
        <button onClick={goBack} className={styles.btnBack}>
            Go Back
        </button>
    );
};
