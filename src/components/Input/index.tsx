import React, {FC} from 'react';
import styles from './style.module.scss';
import classnames from 'classnames';

interface IInputData {
    value: string,
    handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void
    placeholder: string
    error: boolean
}
export const Input:FC<IInputData> = ({value, handleChange, placeholder='Answer', error}) => {

    return (
        <input type="text"
               className={classnames(error ? styles.error : styles.input)}
               id="inputText"
               placeholder={placeholder}
               value={value}
               onChange={handleChange}
               />
    );
};

