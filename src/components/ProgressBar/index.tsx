import React, {FC} from "react";
import styles from './style.module.scss';

interface IProgressBar{
    completed: number
}

export const ProgressBar: FC <IProgressBar> = ({  completed = 50 }) => {

    return (
        <div className={styles.containerStyles}>
            <div className={styles.fillerStyles} style={{width:`${completed}%`}}>
                <span className={styles.labelStyles} >{`${completed}%`}</span>
            </div>
        </div>
);
};
