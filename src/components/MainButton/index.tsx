import React, {FC} from 'react';
import styles from './style.module.scss';

interface IMainButton {
    onClick: () => void,
}

export const MainButton: FC <IMainButton> = ({onClick, children }) => {
    return (
        <button onClick={ onClick } className={styles.startButton}>
            {children}
        </button>
    );
};
