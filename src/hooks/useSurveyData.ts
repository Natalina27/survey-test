import {useEffect, useState} from 'react';
import {api} from '../utils/api';
import {ISurveyData} from '../types';

export const useSurveyData = () => {
    const [survey, setSurvey] = useState<ISurveyData>();

    useEffect(() => {
        (async () => {
            const data = await api.getSurvey();
            setSurvey(data);
        })();
    }, []);

    return {survey};
};

