import React, {useEffect, useState} from 'react';
import {SurveyAllSet, SurveyQA, SurveyStart} from './pages';
import {useSurveyData} from './hooks/useSurveyData';
import styles from './app.module.scss'
import {IQuestions} from './types';


function App() {
    const [open, setOpen] = useState(false);
    const [done, setDone] = useState(false);
    const [questions, setQuestions] = useState<IQuestions>([]);

    const onStart = () => setOpen(true);

    const {survey} = useSurveyData();

    useEffect(() => {
        if (!survey) return;
        const questionsArr = survey.sections.reduce<IQuestions>((prev, curr) => {
            curr.questionIds.forEach(elem => {
                const obj = {
                    section: curr.title,
                    questionId: elem
                };

                prev.push(obj);
            });
            return prev;
        }, []);

        setQuestions(questionsArr);

    }, [survey]);

    const {surveyName = 'Survey Name', description = 'Description'} = survey || {};

    return (
        <main className={styles.main}>
            {!open ? <SurveyStart onStart={onStart}
                                  surveyName={surveyName}
                                  description={description}/>
                : !done ? <SurveyQA questions={questions}
                                    setDone={setDone}/>
                    : <SurveyAllSet/>
            }
        </main>
    )
}

export default App;
